$(document).ready(function() {
	var version='4.3.0';
	
	// Includes
	$('body > header').load('resources/html/includes/header.html?v='+version, function() {
		$('body > footer').load('resources/html/includes/footer.html?v='+version, function() {
			$('body > nav').load('resources/html/includes/menu-top.html?v='+version, function() {
				$('#menu-top > ul > li').each(function() {
					if (window.location.pathname.indexOf($(this).data('active')) >= 0) {
				    	$(this).children('.nav-link').addClass('active');
					}
				});
				$('#menu-top .dropdown-menu > li').each(function() {
					if (window.location.pathname.indexOf($(this).data('active')) >= 0) {
				    	$(this).children('.dropdown-item').addClass('active');
					}
				});
				$('#menu-componenti').load('resources/html/includes/menu-componenti.html?v='+version, function() {
					$('#menu-left > li').each(function() {
						if (window.location.pathname.indexOf($(this).data('active')) >= 0) {
					    	$(this).children('.nav-link').addClass('active');
						}
					});
				});
				selectTheme(sessionStorage.getItem('uikit_theme'));
				$('body').removeAttr('hidden');
			});
		});
	});
	
	// Templates
	$('#tpl-header-siti').load('resources/html/snippets/header-siti.html?v='+version);
	$('#tpl-header-app').load('resources/html/snippets/header-app.html?v='+version);
	$('#tpl-header-app-sm').load('resources/html/snippets/header-app-sm.html?v='+version);
	$('#tpl-menu-top-siti').load('resources/html/snippets/menu-top-siti.html?v='+version);
	$('#tpl-menu-top-app').load('resources/html/snippets/menu-top-app.html?v='+version);
	$('#tpl-menu-top-app-sm').load('resources/html/snippets/menu-top-app-sm.html?v='+version);
	$('#tpl-menu-left-dark').load('resources/html/snippets/menu-left-dark.html?v='+version);
	$('#tpl-menu-left-light').load('resources/html/snippets/menu-left-light.html?v='+version);
	$('#tpl-footer-siti').load('resources/html/snippets/footer-siti.html?v='+version);
	$('#tpl-footer-app').load('resources/html/snippets/footer-app.html?v='+version);
	$('#tpl-footer-app-sm').load('resources/html/snippets/footer-app-sm.html?v='+version);
	
	// Snippets
	$('#snp-header-siti').load('resources/html/snippets/header-siti.html?v='+version);
	$('#snp-header-app').load('resources/html/snippets/header-app.html?v='+version);
	$('#snp-header-app-sm').load('resources/html/snippets/header-app-sm.html?v='+version);
	$('#snp-menu-top-siti').load('resources/html/snippets/menu-top-siti.html?v='+version);
	$('#snp-menu-top-app').load('resources/html/snippets/menu-top-app.html?v='+version);
	$('#snp-menu-top-app-sm').load('resources/html/snippets/menu-top-app-sm.html?v='+version);
	$('#snp-menu-left').load('resources/html/snippets/menu-left-dark.html?v='+version);
	$('#snp-footer-siti').load('resources/html/snippets/footer-siti.html?v='+version);
	$('#snp-footer-app').load('resources/html/snippets/footer-app.html?v='+version);
	$('#snp-footer-app-sm').load('resources/html/snippets/footer-app-sm.html?v='+version);
	
	$('#snp-accordion').load('resources/html/snippets/accordion.html?v='+version);
	$('#snp-bottoni').load('resources/html/snippets/bottoni.html?v='+version);
	$('#snp-button-group').load('resources/html/snippets/button-group.html?v='+version);
	$('#snp-breadcrumb').load('resources/html/snippets/breadcrumb.html?v='+version);
	$('#snp-captcha').load('resources/html/snippets/captcha.html?v='+version);
	$('#snp-card').load('resources/html/snippets/card.html?v='+version);
	$('#snp-card-horizontal').load('resources/html/snippets/card-horizontal.html?v='+version);
	$('#snp-card-icon-bg').load('resources/html/snippets/card-icon-bg.html?v='+version);
	$('#snp-card-image').load('resources/html/snippets/card-image.html?v='+version);
	$('#snp-card-simple').load('resources/html/snippets/card-simple.html?v='+version);
	$('#snp-carousel').load('resources/html/snippets/carousel.html?v='+version);
	$('#snp-chips').load('resources/html/snippets/chips.html?v='+version);
	$('#snp-collapsible').load('resources/html/snippets/collapsible.html?v='+version);
	$('#snp-datepicker').load('resources/html/snippets/datepicker.html?v='+version);
	$('#snp-entrypoint').load('resources/html/snippets/entrypoint.html?v='+version);
	$('#snp-form').load('resources/html/snippets/form.html?v='+version);
	$('#snp-icone').load('resources/html/snippets/icone.html?v='+version);
	$('#snp-immagini-multi').load('resources/html/snippets/immagini-multi.html?v='+version);
	$('#snp-immagini').load('resources/html/snippets/immagini.html?v='+version);
	$('#snp-input').load('resources/html/snippets/input.html?v='+version);
	$('#snp-input-classic').load('resources/html/snippets/input-classic.html?v='+version);
	$('#snp-input-dimensioni').load('resources/html/snippets/input-dimensioni.html?v='+version);
	$('#snp-input-group').load('resources/html/snippets/input-group.html?v='+version);
	$('#snp-badge').load('resources/html/snippets/badge.html?v='+version);
	$('#snp-layout-menu').load('resources/html/snippets/layout-menu.html?v='+version);
	$('#snp-layout').load('resources/html/snippets/layout.html?v='+version);
	$('#snp-link').load('resources/html/snippets/link.html?v='+version);
	$('#snp-liste').load('resources/html/snippets/liste.html?v='+version);
	$('#snp-list-group').load('resources/html/snippets/list-group.html?v='+version);
	$('#snp-main').load('resources/html/snippets/main.html?v='+version);
	$('#snp-main-section').load('resources/html/snippets/main-section.html?v='+version);
	$('#snp-mediaquery').load('resources/html/snippets/mediaquery.html?v='+version);
	$('#snp-messaggi').load('resources/html/snippets/messaggi.html?v='+version);
	$('#snp-messaggi-link').load('resources/html/snippets/messaggi-link.html?v='+version);
	$('#snp-messaggi-multipli').load('resources/html/snippets/messaggi-multipli.html?v='+version);
	$('#snp-modal').load('resources/html/snippets/modal.html?v='+version);
	$('#snp-page').load('resources/html/snippets/page.html?v='+version);
	$('#snp-paginazione').load('resources/html/snippets/paginazione.html?v='+version);
	$('#snp-progress-bar').load('resources/html/snippets/progress-bar.html?v='+version);
	$('#snp-scroll-top').load('resources/html/snippets/scroll-top.html?v='+version);
	$('#snp-spinner').load('resources/html/snippets/spinner.html?v='+version);
	$('#snp-tabelle').load('resources/html/snippets/tabelle.html?v='+version);
	$('#snp-tabs').load('resources/html/snippets/tabs.html?v='+version);
	$('#snp-tabs-reverse').load('resources/html/snippets/tabs-reverse.html?v='+version);
	$('#snp-titoli').load('resources/html/snippets/titoli.html?v='+version);
	$('#snp-tree-view').load('resources/html/snippets/tree-view.html?v='+version);
	$('#snp-tree-view-form').load('resources/html/snippets/tree-view-form.html?v='+version);
	$('#snp-validazione').load('resources/html/snippets/validazione.html?v='+version);
	$('#snp-video').load('resources/html/snippets/video.html?v='+version);
	$('#snp-wizard').load('resources/html/snippets/wizard.html?v='+version);
	
	// Prevent link example
	$('a[href=""]').click(function(e) {
		e.preventDefault();
	});
});

function changeTheme(theme) {
	sessionStorage.setItem('uikit_theme', theme);
	selectTheme(theme);
	$('html, body').animate({scrollTop: 0}, 'slow');
}

function selectTheme(theme) {
	switch (theme) {
	case 'ae':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/agenzia-entrate.css');
		$('header .logo').attr('src', 'resources/img/AE_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/AE_mono_152.png');
		break;
	case 'dm':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/dogane-monopoli.css');
		$('header .logo').attr('src', 'resources/img/ADM_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/ADM_mono_152.png');
		break;
	case 'ad':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/agenzia-demanio.css');
		$('header .logo').attr('src', 'resources/img/AD_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/AD_mono_152.png');
		break;
	case 'gdf':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/guardia-finanza.css');
		$('header .logo').attr('src', 'resources/img/GdF_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/GdF_mono_152.png');
		break;
	case 'sts':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/sistema-ts.css');
		$('header .logo').attr('src', 'resources/img/STS_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/STS_mono_152.png');
		break;
	case 'gt':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/giustizia-tributaria.css');
		$('header .logo').attr('src', 'resources/img/GT_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/GT_mono_152.png');
		break;
	case 'eqg':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/equitalia-giustizia.css');
		$('header .logo').attr('src', 'resources/img/EqG_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/EqG_mono_152.png');
		break;
	case 'pff':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/portale-ff.css');
		$('header .logo').attr('src', 'resources/img/PFF_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/PFF_mono_152.png');
		break;
	case 'it':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/it.css');
		$('.logo').attr('src', 'resources/img/it_logo_152.png');
		break;
	default:
		$('#agenzia-theme').attr('href', 'resources/css/uikit/sogei.css');
		$('header .logo').attr('src', 'resources/img/Sogei_logo_152.png');
		$('footer .logo').attr('src', 'resources/img/Sogei_mono_152.png');
		break;
	}
}