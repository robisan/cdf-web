$(document).ready(function() {
	var version='4.3.0';

	selectTheme(sessionStorage.getItem('uikit_theme'));

	// Configurazione Input Password
	$('.btn-show-hide').click(function() {
		var e = $(this).closest('.form-group');
		if ($(this).children('.fas').hasClass('fa-eye')) {
			e.find('.form-control').attr('type','text');
			$(this).children('.fas').removeClass('fa-eye').addClass('fa-eye-slash');
			$(this).prop('title', 'Nascondi password');
		} else {
			e.find('.form-control').attr('type','password');
			$(this).children('.fas').removeClass('fa-eye-slash').addClass('fa-eye');
			$(this).prop('title', 'Mostra password');
		}
	});

	// snippets
	$('#snp-login').load('resources/html/snippets/login.html?v='+version);
	$('#snp-error').load('resources/html/snippets/error.html?v='+version);
});

function selectTheme(theme) {
	switch (theme) {
	case 'ae':
		$("#agenzia-theme").attr("href", "resources/css/uikit/agenzia-entrate.css");
		$('.logo').attr('src', 'resources/img/AE_logo_152.png');
		break;
	case 'dm':
		$("#agenzia-theme").attr("href", "resources/css/uikit/dogane-monopoli.css");
		$('.logo').attr('src', 'resources/img/ADM_logo_152.png');
		break;
	case 'ad':
		$("#agenzia-theme").attr("href", "resources/css/uikit/agenzia-demanio.css");
		$('.logo').attr('src', 'resources/img/AD_logo_152.png');
		break;
	case 'gdf':
		$("#agenzia-theme").attr("href", "resources/css/uikit/guardia-finanza.css");
		$('.logo').attr('src', 'resources/img/GdF_logo_152.png');
		break;
	case 'sts':
		$("#agenzia-theme").attr("href", "resources/css/uikit/sistema-ts.css");
		$('.logo').attr('src', 'resources/img/STS_logo_152.png');
		break;
	case 'gt':
		$("#agenzia-theme").attr("href", "resources/css/uikit/giustizia-tributaria.css");
		$('.logo').attr('src', 'resources/img/GT_logo_152.png');
		break;
	case 'eqg':
		$("#agenzia-theme").attr("href", "resources/css/uikit/equitalia-giustizia.css");
		$('.logo').attr('src', 'resources/img/EqG_logo_152.png');
		break;
	case 'pff':
		$("#agenzia-theme").attr("href", "resources/css/uikit/portale-ff.css");
		$('.logo').attr('src', 'resources/img/PFF_logo_152.png');
		break;
	case 'it':
		$('#agenzia-theme').attr('href', 'resources/css/uikit/it.css');
		$('.logo').attr('src', 'resources/img/it_logo_152.png');
		break;
	default:
		$("#agenzia-theme").attr("href", "resources/css/uikit/sogei.css");
		$('.logo').attr('src', 'resources/img/Sogei_logo_152.png');
		break;
	}
}