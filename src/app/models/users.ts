import { Authorizations } from './authorizations';

export class User {
    username: string;
    nome: string;
    cognome: string;
    cf: string;
    isAuthenticated: boolean;
    authorizations: Authorizations = new Authorizations();
    session_state: string;
  }