import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEsercentiComponent } from './area-esercenti.component';

describe('AreaEsercentiComponent', () => {
  let component: AreaEsercentiComponent;
  let fixture: ComponentFixture<AreaEsercentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEsercentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEsercentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
