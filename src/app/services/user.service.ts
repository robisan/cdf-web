
import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEvent } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../models/users';



@Injectable({
    providedIn: 'root'
})


export class UserService {

    protected apiUrlRichiedente = environment.API_URL_RICHIEDENTE;
    protected apiUrlEsercente = environment.API_URL_ESERCENTE;

    public defaultHeaders = new HttpHeaders();
    //public configuration = new Configuration();

    constructor(protected httpClient: HttpClient) {
    }


    /**
     * Recupero dettaglio utente
     * Restituisce il dettaglio dell&#39;utente loggato
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public userInfo(observe?: 'body', reportProgress?: boolean): Observable<User>;
    public userInfo(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<User>>;
    public userInfo(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<User>>;
    public userInfo(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (api_key) required
       // if (this.configuration && this.configuration.apiKeys && this.configuration.apiKeys["api_key"]) {
       //     headers = headers.set('api_key', this.configuration.apiKeys["api_key"]);
       // }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
      //  const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
      //  if (httpHeaderAcceptSelected != undefined) {
      //      headers = headers.set('Accept', httpHeaderAcceptSelected);
      //  }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<User>(this.apiUrlEsercente+'/user/getUserInfo',
            {
               // withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
