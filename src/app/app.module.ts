import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { MenuComponent } from './layout/menu/menu.component';

import { MaterialModule } from './material-module';
import { UserService } from './services/user.service';
import { ManagerService } from './services/manager.service';
import { ComeFunzionaComponent } from './come-funziona/come-funziona.component';
import { DoveUsareLaCartaComponent } from './dove-usare-la-carta/dove-usare-la-carta.component';
import { AreaEsercentiComponent } from './area-esercenti/area-esercenti.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    ComeFunzionaComponent,
    DoveUsareLaCartaComponent,
    AreaEsercentiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    MaterialModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [UserService,ManagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
