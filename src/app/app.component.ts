import { Component } from '@angular/core';
import { User } from './models/users';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ManagerService } from '../app/services/manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Cdf-Web';
  isAuthenticated: boolean = false;
  user: User = new User();
  showRouterOutlet: boolean = false;
  showHeaderAndFooter: boolean = false;

  threshold: number = 0;
  timeout: number = 30000;

  constructor(private router: Router,
    private userService: UserService,
    private spinnerService: Ng4LoadingSpinnerService,
    private managerService: ManagerService
  ) {
    this.showRouterOutlet = false;
  }

  ngOnInit() {

    this.spinnerService.show();

    this.userService.userInfo().subscribe(userInfo => {
      this.managerService.currentUser = userInfo;
      //sessionStorage.setItem('funzionalita', JSON.stringify(userInfo.elencoFunzionalita));
      this.showRouterOutlet = true;
      this.isAuthenticated = true;
      this.showHeaderAndFooter = true;
      this.spinnerService.hide();
    }, error => {
      this.manageErrorInRestCall(error);
    });


  }

  private manageErrorInRestCall(error: any, text: any = 'Problema durante il recupero dei dati') {
   // setTimeout(() => this.toastrService.error(text, 'Error', {
   //   timeOut: 3000
   // }));
    console.log(error);
  }



}
