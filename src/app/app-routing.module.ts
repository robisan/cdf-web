import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/home/home.component';
import { ComeFunzionaComponent } from '../app/come-funziona/come-funziona.component';
import { DoveUsareLaCartaComponent } from './dove-usare-la-carta/dove-usare-la-carta.component';
import { AreaEsercentiComponent } from './area-esercenti/area-esercenti.component';


const routes: Routes = [

  {
    path: 'home',
    component: HomeComponent//,
    //canActivate: [AuthGuardAF]
  },
  {
    path: 'comeFunziona',
    component: ComeFunzionaComponent//,
    //canActivate: [AuthGuardAF]
  },
  {
    path: 'doveUsareLaCarta',
    component: DoveUsareLaCartaComponent//,
    //canActivate: [AuthGuardAF]
  },
  {
    path: 'areaEsercenti',
    component: AreaEsercentiComponent//,
    //canActivate: [AuthGuardAF]
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


