import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {

  navLinks: any[];
  activeLinkIndex = -1; 
  @Input() showRouterOutlet: boolean;

  


  constructor(private router: Router) { 

    this.navLinks = [
      {
          label: 'Homepage',
          link: './home',
          index: 0
      }, {
          label: 'Come funziona',
          link: './comeFunziona',
          index: 1
      }, {
          label: 'Dove usare la carta',
          link: './doveUsareLaCarta',
          index: 2
      }, {
        label: 'Area esercenti',
        link: './areaEsercenti',
        index: 3
    }
  ];

  }

  ngOnInit() {

    this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  });
  }

}
