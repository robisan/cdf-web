import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComeFunzionaComponent } from './come-funziona.component';

describe('ComeFunzionaComponent', () => {
  let component: ComeFunzionaComponent;
  let fixture: ComponentFixture<ComeFunzionaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComeFunzionaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComeFunzionaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
