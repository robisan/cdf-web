import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoveUsareLaCartaComponent } from './dove-usare-la-carta.component';

describe('DoveUsareLaCartaComponent', () => {
  let component: DoveUsareLaCartaComponent;
  let fixture: ComponentFixture<DoveUsareLaCartaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoveUsareLaCartaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoveUsareLaCartaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
