export const environment = {
  production: true,
  API_URL_RICHIEDENTE: '/cdf/utente/release/web/rest',
  API_URL_ESERCENTE: '/cdf/esercente/release/web/rest'
};
